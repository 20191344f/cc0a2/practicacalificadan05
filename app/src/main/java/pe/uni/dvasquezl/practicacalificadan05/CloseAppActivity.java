package pe.uni.dvasquezl.practicacalificadan05;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class CloseAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_app);
    }
}