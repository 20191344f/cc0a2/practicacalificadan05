package pe.uni.dvasquezl.practicacalificadan05;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class GuessAnimalActivity extends AppCompatActivity {

    private static final int SPEECH_REQUEST_CODE = 0;
    private static final int LENGTH = 11;

    TextView textViewScore;
    TextView textViewAnimalToGuess;
    ImageButton imageButtonSpeechRecognizer;
    ImageButton imageButtonChecker;
    ImageButton imageButtonNext;

    ImageView imageViewGuessAnimal;
    String nameAnimalToGuess;

    ArrayList<String> arrayText;
    ArrayList<Integer> arrayImages;
    ArrayList<Integer> randomIndicators = new ArrayList<>();


    int indicator = 0;
    int punctuation = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess_animal);

        imageButtonSpeechRecognizer = findViewById(R.id.button_speech_recognizer);
        imageButtonNext = findViewById(R.id.button_next);
        imageButtonChecker = findViewById(R.id.button_checker);

        textViewScore = findViewById(R.id.text_view_punctuation);
        textViewAnimalToGuess = findViewById(R.id.text_view_animal_to_guess_input);
        imageViewGuessAnimal = findViewById(R.id.image_guess_animal);

        fillArrays();
        randomize();

        nameAnimalToGuess = arrayText.get(randomIndicators.get(indicator));
        imageViewGuessAnimal.setImageResource(arrayImages.get(randomIndicators.get(indicator)));
        indicator++;

        Resources res = getResources();
        textViewAnimalToGuess.setText("");
        textViewScore.setText(String.format(res.getString(R.string.score), punctuation));

        imageButtonSpeechRecognizer.setOnClickListener(view -> displaySpeechRecognizer());
        imageButtonNext.setOnClickListener(view -> {
            punctuation = Math.max(punctuation - 5, 0);
            continueNextImage();
        });

        imageButtonChecker.setOnClickListener(view -> {
            Locale locale = new Locale("es", "ES");
            Collator collator = Collator.getInstance(locale);
            collator.setStrength(Collator.PRIMARY);

            if (collator.compare(nameAnimalToGuess, textViewAnimalToGuess.getText().toString()) == 0) {
                punctuation += 10;
                continueNextImage();
            } else {
                LinearLayout guessAnimal;
                guessAnimal = findViewById(R.id.guess_animal);
                textViewAnimalToGuess.setText("");
                Snackbar.make(guessAnimal, R.string.wrong_guess, Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void continueNextImage() {
        if (indicator < LENGTH) {
            nameAnimalToGuess = arrayText.get(randomIndicators.get(indicator));
            imageViewGuessAnimal.setImageResource(arrayImages.get(randomIndicators.get(indicator)));
            textViewAnimalToGuess.setText("");

            Resources res = getResources();
            textViewScore.setText(String.format(res.getString(R.string.score), punctuation));
            indicator++;
        } else {
            Intent intent = new Intent(GuessAnimalActivity.this, FinishesGameActivity.class);
            intent.putExtra("SCORE", punctuation);
            startActivity(intent);
            finish();
        }
    }

    private void randomize() {

        System.out.println("generando array aleatorio");
        ArrayList<Integer> result = new ArrayList<>();

        for (int i = 0; i < LENGTH; i++) {
            result.add(i);
        }

        Collections.shuffle(result);
        randomIndicators = result;
        System.out.println("generando array aleatorio" + result);
    }

    private void fillArrays() {
        arrayText = new ArrayList<>();
        arrayImages = new ArrayList<>();
        randomIndicators = new ArrayList<>();

        arrayText.add("Abeja");
        arrayText.add("Gato");
        arrayText.add("Cocodrilo");
        arrayText.add("Perro");
        arrayText.add("Perro");
        arrayText.add("Leon");
        arrayText.add("Mono");
        arrayText.add("Pingüino");
        arrayText.add("Conejo");
        arrayText.add("Obeja");
        arrayText.add("Serpiente");

        arrayImages.add(R.drawable.bee);
        arrayImages.add(R.drawable.cat);
        arrayImages.add(R.drawable.crocodile);
        arrayImages.add(R.drawable.dog);
        arrayImages.add(R.drawable.dog_brow);
        arrayImages.add(R.drawable.lion);
        arrayImages.add(R.drawable.monkey);
        arrayImages.add(R.drawable.penguin);
        arrayImages.add(R.drawable.rabbit);
        arrayImages.add(R.drawable.sheep);
        arrayImages.add(R.drawable.sneak);

        System.out.println("generando array" + arrayText);
        System.out.println("generando array" + arrayImages);
    }

    // Create an intent that can start the Speech Recognizer activity
    @SuppressWarnings("deprecation")
    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        // This starts the activity and populates the intent with the speech text.
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    // This callback is invoked when the Speech Recognizer returns.
    // This is where you process the intent and extract the speech text from the intent.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            // Do something with spokenText.
            textViewAnimalToGuess.setText(spokenText.toUpperCase());
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}