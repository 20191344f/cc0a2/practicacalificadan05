package pe.uni.dvasquezl.practicacalificadan05;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Button buttonReadText;
    Button buttonGuessAnimal;
    Button buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonReadText = findViewById(R.id.main_button_read_text);
        buttonGuessAnimal = findViewById(R.id.main_button_guess_animal);
        buttonExit = findViewById(R.id.main_button_exit);

        buttonReadText.setOnClickListener(view -> Toast.makeText(getApplicationContext(), R.string.dialog_functionality_noimplement, Toast.LENGTH_LONG).show());

        buttonGuessAnimal.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, GuessAnimalActivity.class);
            startActivity(intent);
        });

        buttonExit.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(R.string.dialog_exit_title);
            builder.setCancelable(false);
            Resources res = getResources();
            builder.setMessage(res.getString(R.string.dialog_exit_message));
            builder.setPositiveButton("Si", (dialogInterface, i) -> {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
            });
            builder.setNegativeButton("No", (dialogInterface, i) -> {});
            builder.create().show();
        });
    }
}