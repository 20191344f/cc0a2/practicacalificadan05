package pe.uni.dvasquezl.practicacalificadan05;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class FinishesGameActivity extends AppCompatActivity {

    TextView textViewScore;
    ImageButton imageButtonRepite;
    ImageButton imageButtonContinue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finishes_game);

        int score = getIntent().getIntExtra("SCORE", 0);

        textViewScore = findViewById(R.id.text_view_final_score);
        imageButtonRepite = findViewById(R.id.button_repite);
        imageButtonContinue = findViewById(R.id.button_continue);

        Resources res = getResources();
        textViewScore.setText(String.format(res.getString(R.string.score), score));



        imageButtonContinue.setOnClickListener(view -> {
            Intent intent = new Intent(FinishesGameActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        });

        imageButtonRepite.setOnClickListener(view -> {
            Intent intent = new Intent(FinishesGameActivity.this, GuessAnimalActivity.class);
            startActivity(intent);
            finish();
        });


    }
}